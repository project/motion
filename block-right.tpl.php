<?php
?>
<li id="block-<?php print $block->module . '-' . $block->delta; ?>" class="boxed widget block-<?php print $block->module ?>">
  <?php if ($block->subject): ?>
    <h3 class="widgettitle"><?php print $block->subject ?></h3>
  <?php endif; ?>	
  <?php print $block->content ?>
</li>