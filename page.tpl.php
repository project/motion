<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head profile="http://gmpg.org/xfn/11">
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>

<!-- for translations -->

<!--[if lt IE 7]>
<link href="<?php  print base_path() . path_to_theme() ?>/ie6.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php  print base_path() . path_to_theme() ?>/js/unitpngfix.js"></script>
<![endif]--> 

<!--[if IE 7]>
<link href="<?php  print base_path() . path_to_theme() ?>/ie7.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]--> 


</head>

<body>
  <div id="wrapper">
	<div id="top">
	  
	    
          <?php print $s_links; ?>

      
      
      <?php if ($search_box): ?>
        <div id="search">
          <?php print $search_box; ?>
        </div>
      <?php endif ?>

      <div class="cleared"></div>
    </div><!-- /top -->
    <div id="header">
	  <div id="logo">
		<a href="<?php print $front_page; ?>"><img src="<?php  print base_path() . path_to_theme() ?>/images/genericlogo.png" alt="<?php print $site_name; ?>" /></a>
        <h1><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>
        <?php if ($site_slogan): ?><div id="desc"><?php print $site_slogan; ?></div><?php endif; ?>
        
      </div><!-- /logo -->  
      <?php if ($is_front && !empty($mission)): ?>
        <div id="headerbanner">
          <p><?php print $mission; ?></p>
        </div>
      <?php endif; ?>
      <div class="cleared"></div>
    </div><!-- /header -->

    <div id="catnav">
      <?php print $p_links; ?>
      <div class="cleared"></div>
    </div>
  <div id="main">
    <div id="content">
      <?php if (!empty($breadcrumb)): ?><div class="drupal-breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
   	  <?php if (!empty($title) && !$node): ?><h2 ><?php print $title; ?></h2><?php endif; ?>
      <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
   	  <?php if (!empty($messages)): print $messages; endif; ?>
      <?php if (!empty($help)): print $help; endif; ?>
      <?php print $content; ?>
    </div><!-- /content -->
    <div id="sidebar">
      <?php if ($right): ?>
        <ul>
          <?php print $right; ?>
        </ul>
      <?php endif; ?>
    </div><!-- /sidebar -->    
    <div class="cleared"></div>
  </div><!-- /main --> 

<div id="footer">
  <?php print $footer; ?>     
  <div class="cleared"></div>
</div>
<div id="credits">
  <div id="creditsleft">
    <a href="http://85ideas.com/public-releases/wordpress-theme-motion/">Motion</a> theme ported by top <a href="http://topdrupalthemes.net">drupal themes</a>.<br />
    <?php print $footer_msg; ?>
  </div>
  <div id="creditsright">
    <a href="#main">&#91; <?php print t('Back to top'); ?> &#93;</a>
  </div><!-- /creditsright -->
  <div class="cleared"></div>
</div><!-- /credits -->
</div><!-- /wrapper -->
<?php print $closure; ?>
</body>
</html>
